package com.magic.unittestsrxjava

interface BaseView<T> {
    fun setPresenter(presenter: T)
}