package com.magic.unittestsrxjava.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.magic.unittestsrxjava.R
import com.magic.unittestsrxjava.data.Topics
import kotlinx.android.synthetic.main.theme_fragment.*

class MainTopicFragment : Fragment(), TopicContract.View, TopicAdapter.MyClickListener {

    var mListTopics: List<Topics>? = null
    private var mPresenter: TopicContract.Presenter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.theme_fragment, container, false)
        mPresenter!!.subscribe()
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        button_here.setOnClickListener {
//            theme_fragment_progress!!.visibility = View.VISIBLE
//            button_here!!.visibility = View.GONE
//            mPresenter!!.fetch()
//        }
    }

    override fun showTopics(list: List<Topics>) {
        mListTopics = list
        theme_fragment_recycler_view!!.setHasFixedSize(true)
        theme_fragment_recycler_view!!.setLayoutManager(LinearLayoutManager(getActivity()))
        val adapter = MainTopicsAdapter(activity!!, list!!)
        theme_fragment_recycler_view!!.setAdapter(adapter)
    }

    override fun showError() {
        theme_fragment_progress!!.visibility = View.GONE
        button_here!!.visibility = View.VISIBLE
        button_here!!.setText(getString(R.string.retry))
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (!active) {
            button_here!!.visibility = View.GONE
            theme_fragment_progress!!.visibility = View.GONE
        }
    }

    override fun onPause() {
        super.onPause()
        mPresenter!!.unSubscribe()
    }

    companion object {
        fun newInstance(): MainTopicFragment {
            return MainTopicFragment()
        }
    }

    override fun setPresenter(presenter: TopicContract.Presenter) {
        mPresenter = presenter
    }

    override fun onItemClick(position: Int, v: View?) {

    }

}