package com.magic.unittestsrxjava.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.magic.unittestsrxjava.R
import com.magic.unittestsrxjava.data.Topics

class TopicAdapter(
    private val mContext: Context,
    list: List<Topics>
) :
    RecyclerView.Adapter<TopicAdapter.DataObjectHolder?>() {
    private val mList: List<Topics>
    // Allows to remember the last item shown on screen
    private var lastPosition = -1

    class DataObjectHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        @BindView(R.id.theme_item_name)
        var mName: Button? = null

        override fun onClick(v: View) {
            sClickListener!!.onItemClick(getAdapterPosition(), v)
        }

        init {
            ButterKnife.bind(this, itemView!!)
            mName!!.setOnClickListener(this)
        }
    }

    fun setOnItemClickListener(myClickListener: MyClickListener) {
        sClickListener = myClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataObjectHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return DataObjectHolder(view)
    }

    override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
        holder.mName!!.setText(mList[position].name)
        // Here you apply the animation when the view is bound
        setAnimation(holder.itemView, position)
    }

    /**
     * Here is the key method to apply the animation
     */
    private fun setAnimation(
        viewToAnimate: View,
        position: Int
    ) { // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation =
                AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    interface MyClickListener {
        fun onItemClick(position: Int, v: View?)
    }

    companion object {
        private var sClickListener: MyClickListener? = null
    }

    init {
        mList = list
    }

    override fun getItemCount(): Int {
        return mList.size
    }
}