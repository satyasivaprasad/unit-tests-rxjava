package com.magic.unittestsrxjava.ui

import com.magic.unittestsrxjava.BasePresenter
import com.magic.unittestsrxjava.BaseView
import com.magic.unittestsrxjava.data.Topics

interface TopicContract {
    interface Presenter : BasePresenter {
        fun fetch()
    }

    interface View : BaseView<Presenter> {
        fun showTopics(list: List<Topics>)
        fun showError()
        fun setLoadingIndicator(active: Boolean)
    }
}