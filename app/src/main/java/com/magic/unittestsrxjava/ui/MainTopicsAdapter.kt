package com.magic.unittestsrxjava.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.magic.unittestsrxjava.R
import com.magic.unittestsrxjava.data.Topics

class MainTopicsAdapter(private val mContext: Context, var list: List<Topics>) :
    RecyclerView.Adapter<MainTopicsAdapter.DataObjectHolder?>() {
    class DataObjectHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val view = itemView
        val mName = itemView.findViewById<Button>(R.id.theme_item_name)
        override fun onClick(v: View?) {

        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainTopicsAdapter.DataObjectHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return MainTopicsAdapter.DataObjectHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MainTopicsAdapter.DataObjectHolder, position: Int) {
        holder.mName!!.setText(list[position].name)
    }
}