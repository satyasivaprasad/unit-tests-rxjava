package com.magic.unittestsrxjava.ui

import androidx.annotation.NonNull
import com.google.common.base.Preconditions
import com.magic.unittestsrxjava.data.RemoteDataSource
import com.magic.unittestsrxjava.data.Topics
import com.magic.unittestsrxjava.utils.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


class TopicPresenter(@NonNull remoteDataSource: RemoteDataSource, @NonNull view: TopicContract.View, @NonNull provider: BaseSchedulerProvider) :
    TopicContract.Presenter {
    @NonNull
    private val mView: TopicContract.View
    @NonNull
    private val mSchedulerProvider: BaseSchedulerProvider

    private val disposables = CompositeDisposable()


    @NonNull
    private val mRemoteDataSource: RemoteDataSource

    override fun fetch() {
        val subscription: Disposable = mRemoteDataSource.topicsRx
            .subscribeOn(mSchedulerProvider.computation())
            .observeOn(mSchedulerProvider.ui())
            .subscribe({ listTopics: List<Topics> ->
                mView.setLoadingIndicator(false)
                mView.showTopics(listTopics)
            },
                { error: Throwable? ->
                    try {
                        mView.showError()
                    } catch (t: Throwable) {
                        throw IllegalThreadStateException()
                    }
                },
                {})
        disposables.add(subscription)
    }

    override fun subscribe() {
        fetch()
    }

    override fun unSubscribe() {
        disposables.clear()
    }

    init {
        mRemoteDataSource =
            Preconditions.checkNotNull(remoteDataSource, "remoteDataSource")
        mView = Preconditions.checkNotNull(view, "view cannot be null!")
        mSchedulerProvider = Preconditions.checkNotNull(provider, "schedulerProvider cannot be null")
        mView.setPresenter(this)
    }
}