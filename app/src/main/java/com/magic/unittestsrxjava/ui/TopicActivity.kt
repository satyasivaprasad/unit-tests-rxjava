package com.magic.unittestsrxjava.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.magic.unittestsrxjava.R
import com.magic.unittestsrxjava.UnitTestingDemoApplication
import com.magic.unittestsrxjava.data.RemoteDataSource
import com.magic.unittestsrxjava.utils.BaseSchedulerProvider
import com.magic.unittestsrxjava.utils.Utils.addFragmentToActivity
import javax.inject.Inject

class TopicActivity : AppCompatActivity() {
    @Inject
    lateinit var mRemoteDataSource: RemoteDataSource
    @Inject
    lateinit var mSchedulerProvider: BaseSchedulerProvider

    override protected fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.theme_activity)
        initializeDagger()
        initFragment()
    }

    private fun initializeDagger() {
        val app: UnitTestingDemoApplication = getApplication() as UnitTestingDemoApplication
        app.appComponent!!.inject(this)
    }

    private fun initFragment() {
        var themeFragment: MainTopicFragment? = null
        val fragment = getSupportFragmentManager().findFragmentById(R.id.theme_activity_contentFrame)
        if (null == fragment) {
            themeFragment = MainTopicFragment.newInstance()
            addFragmentToActivity(supportFragmentManager, themeFragment, R.id.theme_activity_contentFrame)
        }
        TopicPresenter(mRemoteDataSource, themeFragment!!, mSchedulerProvider)
    }
}