package com.magic.unittestsrxjava

import com.magic.unittestsrxjava.ui.TopicActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(topicActivity: TopicActivity?)
}