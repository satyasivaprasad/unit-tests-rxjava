package com.magic.unittestsrxjava

import android.app.Application

class UnitTestingDemoApplication : Application() {
    var mAppComponent: AppComponent? = null
    override fun onCreate() {
        super.onCreate()

        mAppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    val appComponent: AppComponent?
        get() = mAppComponent
}