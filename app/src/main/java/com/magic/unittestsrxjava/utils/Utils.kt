package com.magic.unittestsrxjava.utils

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.common.base.Preconditions

object Utils {
    fun addFragmentToActivity(
        @NonNull fragmentManager: FragmentManager,
        @NonNull fragment: Fragment, frameId: Int
    ) {
        Preconditions.checkNotNull<Any>(fragmentManager)
        Preconditions.checkNotNull<Any>(fragment)
        val transaction: FragmentTransaction = fragmentManager.beginTransaction()
        transaction.add(frameId, fragment)
        transaction.commit()
    }
}
