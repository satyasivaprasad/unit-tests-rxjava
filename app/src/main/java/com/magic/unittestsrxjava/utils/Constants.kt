package com.magic.unittestsrxjava.utils

object Constants {
    const val URL_BASE = "https://guessthebeach.herokuapp.com/api/"
}