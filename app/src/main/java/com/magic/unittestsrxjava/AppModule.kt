package com.magic.unittestsrxjava

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.magic.unittestsrxjava.data.RemoteDataSource
import com.magic.unittestsrxjava.utils.BaseSchedulerProvider
import com.magic.unittestsrxjava.utils.Constants.URL_BASE
import com.magic.unittestsrxjava.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(discernApplication: UnitTestingDemoApplication) {
    var mDiscernApplication: UnitTestingDemoApplication
    @Singleton
    @Provides
    fun provideRemoteDataSource(): RemoteDataSource {
        return RemoteDataSource(
            Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        )
    }

    @Singleton
    @Provides
    fun provideSchedulerProvider(): BaseSchedulerProvider {
        return SchedulerProvider()
    }

    init {
        mDiscernApplication = discernApplication
    }
}