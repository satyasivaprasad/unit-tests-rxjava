package com.magic.unittestsrxjava

interface BasePresenter {
    fun subscribe()
    fun unSubscribe()
}