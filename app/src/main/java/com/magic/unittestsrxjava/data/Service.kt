package com.magic.unittestsrxjava.data

import io.reactivex.Observable
import retrofit2.http.GET

interface Service {
    @get:GET("topics/")
    val topicsRx: Observable<List<Topics>>
}