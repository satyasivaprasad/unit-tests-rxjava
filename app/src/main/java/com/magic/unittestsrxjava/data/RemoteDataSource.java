package com.magic.unittestsrxjava.data;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class RemoteDataSource implements Service {

    private Service api;

    public RemoteDataSource(Retrofit retrofit) {


        this.api = retrofit.create(Service.class);
    }


    @Override
    public Observable<List<Topics>> getTopicsRx() {
        return api.getTopicsRx();
    }
}
