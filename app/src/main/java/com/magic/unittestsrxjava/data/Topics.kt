package com.magic.unittestsrxjava.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Topics(
    @field:Expose @field:SerializedName("id") var id: Int, @field:Expose @field:SerializedName(
        "name"
    ) var name: String
) {

    companion object {
        fun create(id: Int, name: String): Topics {
            return Topics(id, name)
        }
    }

}